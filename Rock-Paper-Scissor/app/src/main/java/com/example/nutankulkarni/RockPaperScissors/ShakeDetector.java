package com.example.nutankulkarni.RockPaperScissors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class ShakeDetector implements SensorEventListener {

    private OnShakeListener mListener;
    private long mtime;

    long GetCurrentTime(){
        return System.currentTimeMillis();
    }

    public void setOnShakeListener(OnShakeListener listener) {
        this.mListener = listener;
    }

    public interface OnShakeListener {
        public void onShake();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public boolean checkShakeThreshold(float strength){
        if(strength > 2.7F) {
            return true;
        }
        return false;
    }

    public float coordinateSquare(float coordinate){
        return coordinate*coordinate;
    }

    public boolean ignoreMinuteShakes(long ctime){
        if (this.mtime + 500 > ctime) {
            return true;
        }
        return false;
    }

    public float divideWithGravity(float coordinate){
        return coordinate / SensorManager.GRAVITY_EARTH;
    }

    public void onSensorChanged(SensorEvent event) {

        float X = event.values[0];
        float Y = event.values[1];
        float Z = event.values[2];

        X = divideWithGravity(X);
        Y = divideWithGravity(Y);
        Z = divideWithGravity(Z);

        float acceleration = (float)Math.sqrt(coordinateSquare(X) + coordinateSquare(Y) + coordinateSquare(Z));

        if (checkShakeThreshold(acceleration)) {
            long currentTime = GetCurrentTime();
            // ignore negligible shakes
            if(ignoreMinuteShakes(currentTime)) {
                return;
            }
            mtime = currentTime;
            mListener.onShake();
        }
    }

}