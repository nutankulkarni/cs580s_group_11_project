package com.example.nutankulkarni.RockPaperScissors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText editText, editText2;
    TextView textViewHumanScore, textViewComputerScore;
    ImageView humanChoiceImage, computerChoiceImage;
    SpeechRecognizer mSpeechRecognizer;
    Intent mSpeechRecognizerIntent;
    int playerScore = 0;
    int computerScore = 0;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private SensorEventListener mGyroscopeEventListener;
    private ShakeDetector mShakeDetector;

    private Context mContext;
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toast.makeText(MainActivity.this, "Welcome!",
                Toast.LENGTH_LONG).show();

        //checkPermission();

        //
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        mContext = this;
        mActivity = this;

        createShakeDetector();
        //

        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        textViewHumanScore = findViewById(R.id.textViewHumanScore);
        textViewComputerScore = findViewById(R.id.textViewCompScore);

        textViewHumanScore.setText("0");
        textViewComputerScore.setText("0");

        humanChoiceImage = findViewById(R.id.humanChoiceImage);
        computerChoiceImage = findViewById(R.id.computerChoiceImage);

        mGyroscopeEventListener = new SensorEventListener(){
            @Override
            public void onSensorChanged(SensorEvent event){
                if(event.values[2] > 10f){
                    getWindow().getDecorView().setBackgroundColor(Color.parseColor("#64d8fe"));
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor,int i){

            }

        };

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.getDefault());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {

            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {
                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String player = matches.get(0).toLowerCase();
                String computer = new String();
                int winFlag = 0;



                if (matches!= null)
                {
                    if(player.contains("rock") || player.contains("paper") || player.contains("scissors"))
                    {
                        editText.setText(matches.get(0));
                        Toast.makeText(MainActivity.this, "You Said! "+ player,
                                Toast.LENGTH_LONG).show();

                        if(player.contains("rock")){
                            humanChoiceImage.setImageResource(R.drawable.rock);
                        }
                        if(player.contains("paper")){
                            humanChoiceImage.setImageResource(R.drawable.paper);
                        }
                        if(player.contains("scissors")){
                            humanChoiceImage.setImageResource(R.drawable.scissor);
                        }

                        int rand;
                        Random r = new Random();
                        rand = r.nextInt(3);
                        if(rand == 0) {
                            computer = "rock";
                            editText2.setText("Rock");
                            computerChoiceImage.setImageResource(R.drawable.rock);
                        }
                        if (rand == 1) {
                            computer = "paper";
                            editText2.setText("Paper");
                            computerChoiceImage.setImageResource(R.drawable.paper);
                        }
                        if(rand == 2) {
                            computer = "scissors";
                            editText2.setText("Scissors");
                            computerChoiceImage.setImageResource(R.drawable.scissor);
                        }

                        //Paper covers rock
                        if(player.equals("paper") && computer.equals("rock")){
                            playerScore++;
                            winFlag = 1;
                        }
                        if(player.equals("rock") && computer.equals("paper")) {
                            computerScore++;
                            winFlag = 2;
                        }

                        //Rock beats Scissors
                        if(player.equals("rock") && computer.equals("scissors")){
                            playerScore++;
                            winFlag = 1;
                        }
                        if(player.equals("scissors") && computer.equals("rock")){
                            computerScore++;
                            winFlag = 2;
                        }

                        //Scissor cuts paper
                        if(player.equals("scissors") && computer.equals("paper")){
                            playerScore++;
                            winFlag = 1;
                        }
                        if(player.equals("paper") && computer.equals("scissors")){
                            computerScore++;
                            winFlag = 2;
                        }

                        //Both are equal
                        if(player.equals(computer)){
                            winFlag = 3;
                        }


                        if(winFlag == 0) {
                            Toast.makeText(MainActivity.this, "Try Again...",
                                    Toast.LENGTH_LONG).show();
                        }
                        if(winFlag == 1) {
                            Toast.makeText(MainActivity.this, "Human Won!",
                                    Toast.LENGTH_LONG).show();
                        }
                        if(winFlag == 2) {
                            Toast.makeText(MainActivity.this, "Computer Won!",
                                    Toast.LENGTH_LONG).show();
                        }
                        if(winFlag == 3) {
                            Toast.makeText(MainActivity.this, "Its a tie!",
                                    Toast.LENGTH_LONG).show();
                        }

                       textViewHumanScore.setText(String.valueOf(playerScore));
                       textViewComputerScore.setText(String.valueOf(computerScore));
                    }
                    else{
                        humanChoiceImage.setImageResource(R.drawable.question_mark);
                        computerChoiceImage.setImageResource(R.drawable.question_mark);
                        Toast.makeText(MainActivity.this, "Invalid word",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });

       findViewById(R.id.button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_UP:
                        mSpeechRecognizer.stopListening();
                        editText.setHint("Processing");
                        //editText.setHint("You will se the input here");
                        break;
                    case MotionEvent.ACTION_DOWN:
                        //editText.setText(" ");
                        //editText.setText("Edited");
                        editText.setHint("Listening");
                        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                        break;

                }
                return false;
            }
        });
    }

    //
    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,    SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(mGyroscopeEventListener, mGyroscope,    SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onPause() {
        mSensorManager.unregisterListener(mShakeDetector);
        mSensorManager.unregisterListener(mGyroscopeEventListener);
        super.onPause();
    }

    void createShakeDetector(){
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake() {
                Toast.makeText(mContext, "Resetting Game!",
                        Toast.LENGTH_SHORT).show();

                editText.setText(" ");
                editText2.setText(" ");
                textViewHumanScore.setText("0");
                textViewComputerScore.setText("0");
                playerScore = 0;
                computerScore = 0;
                humanChoiceImage.setImageResource(R.drawable.question_mark);
                computerChoiceImage.setImageResource(R.drawable.question_mark);
                getWindow().getDecorView().setBackgroundColor(Color.WHITE);

            }
        });
    }
    //

    private void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)){
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }
    }
}